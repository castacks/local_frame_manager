#ifndef LATLONVISUALIZER_H
#define LATLONVISUALIZER_H
#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "tf_utils/tf_utils.h"
#include "local_frame_manager/local_frame_manager.h"
#include "vector"
#include "dem_reader/dem.h"
#include "tf/transform_broadcaster.h"

class LatLonFramePublisher
{    
    std::vector<geometry_msgs::Point> pvector_;
    std::vector<double> yaw_vector_;
    std::vector<std::string> base_frame_vector_;
	std::vector<std::string> frame_vector_;  
    tf::TransformBroadcaster tf_br_;
    DEMData dem_reader_;

public:
    LocalFrameManager lfm_client_;
    LatLonFramePublisher(){}
    bool Initialize(ros::NodeHandle& n);
    bool PutLatLonPointsOnGround();
    void PublishFrames();
};

#endif // LATLONVISUALIZER_H
