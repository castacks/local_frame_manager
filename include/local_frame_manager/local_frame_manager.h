#ifndef LOCAL_FRAME_MANAGER_LOCALFRAMEMANAGER_H
#define LOCAL_FRAME_MANAGER_LOCALFRAMEMANAGER_H
#include "ros/ros.h"
#include "math_utils/math_utils.h"
#include "latlon_utm_converter/coordconv.h"
#include "nav_msgs/Odometry.h"
#include "tf_utils/tf_utils.h"
#include "dem_reader/dem.h"
#include "local_frame_manager/LatLonAlt.h"
#include "local_frame_manager/FrameManagerParams.h"

class LocalFrameManager
{
private:
    double local_frame_utm_x_;
    double local_frame_utm_y_;
    double local_frame_msl_;

    double local_frame_lat_; //RADIANS
    double local_frame_lon_; //RADIANS
    double local_frame_altitude_;

    std::string local_static_frame_;
    ros::Subscriber lat_lon_subscriber_;
    ros::Subscriber odometry_subscriber_;
    ros::Subscriber frame_manager_params_subscriber_;

    ros::Publisher frame_manager_params_publisher_;

    nav_msgs::Odometry last_received_odo_;
    local_frame_manager::LatLonAlt last_received_latlon_;
    double message_time_tolerance_;
    ca::CoordinateConversion coord_conv_;
    tf::TransformListener tf_listener_;

    bool Setup();
    bool DoMessagesTimesMatch();
    bool initialized_;

    int utm_zone_;
    char hemisphere_;
    bool use_dem_;

    DEMData* dem_reader_;
    ros::Timer FramePubTimer;

    void LatLonCallback(const local_frame_manager::LatLonAlt::ConstPtr& msg);
    void OdoCallback(const nav_msgs::Odometry::ConstPtr& msg);
    void FrameManagerCallback(const local_frame_manager::FrameManagerParams::ConstPtr& msg);
    bool SetUpFrameManagerParams(std::string frame, Eigen::Vector3d& pos, double lat, double lon, double altitude,
                                                    int UTMZone, char hemisphere, double time_tolerance);
    void PublishFrameManagerParamsTimer(const ros::TimerEvent& event);
    void PublishFrameManagerParams();
    bool SetupLocalFrameUsingParams(ros::NodeHandle &n, bool use_dem);
public:
    bool MSL2Altitude(double msl, double& altitude){
        if(!initialized_) return false;
        altitude = local_frame_altitude_ - (msl - local_frame_msl_);
        return true;
    }
    bool Altitude2MSL(double alt, double& msl){
        if(!initialized_) return false;
        msl = local_frame_msl_ - (alt - local_frame_altitude_);
        return true;
    };
    bool LatLon2UTM(double lat, double lon, double altitude ,Eigen::Vector3d& utm);
    bool UTM2LocalStaticFrame(Eigen::Vector3d& utm ,Eigen::Vector3d &local_frame);
    bool LocalStaticFrame2UTM(Eigen::Vector3d& local_frame ,Eigen::Vector3d &utm);
    bool Initialize(ros::NodeHandle &n, bool server);
    bool IsInitialized(){ return initialized_;}
    std::string get_local_static_frame(){ return local_static_frame_;}

    bool TransformLatLon2LocalFrame(double lat, double lon, double altitude,
                                    Eigen::Vector3d &pos_in_local_frame,
                                    std::string local_frame);

    bool TransformLocalFrame2LatLon(Eigen::Vector3d pos_in_local_frame,
                                    std::string local_frame,
                                    double &lat, double &lon, double &altitude);

    bool Initialize(bool server, ros::NodeHandle &n);
    LocalFrameManager();
    ~LocalFrameManager();
};

#endif // LOCALFRAMEMANAGER_H
