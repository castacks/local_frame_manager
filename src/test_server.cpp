#include "ros/ros.h"
#include "local_frame_manager/local_frame_manager.h"


int main(int argc, char **argv)
{

  ros::init(argc, argv, "local_frame_manager");
  ros::NodeHandle n("~");
  //std::string image_topic= "/front_cam_left/camera_left/image";
  //ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "/testing_camera_interface/camera_rays", 0 );
  LocalFrameManager fm;
  bool server = true;
  fm.Initialize(server,n);
  ros::Rate r(10); // 10 hz

  while(ros::ok()){
    ros::spinOnce();
    r.sleep();
  }
  return 0;
}

