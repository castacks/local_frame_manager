﻿#include "local_frame_manager/local_frame_manager.h"

LocalFrameManager::LocalFrameManager(){
    dem_reader_ = new DEMData();
}

LocalFrameManager::~LocalFrameManager(){}

bool LocalFrameManager::LatLon2UTM(double lat, double lon, double altitude ,Eigen::Vector3d& utm){
    if(!initialized_){
        ROS_ERROR_STREAM("LocalFrameManager is not intialized.");
        return false;
    }
    double utm_x, utm_y;
    if(coord_conv_.LatLongRad2UTM(lat,lon, &utm_x, &utm_y)<0){
        ROS_ERROR_STREAM("LocalFrameManager could not transform lat lon to UTM.");
        return false;
    }

    double msl_height = -1.0*(altitude - local_frame_altitude_) + local_frame_msl_;

    utm.x() = utm_x; utm.y() = utm_y; utm.z() = msl_height;
    return true;
}

bool LocalFrameManager::UTM2LocalStaticFrame(Eigen::Vector3d& utm ,Eigen::Vector3d &pos_local_frame){
    if(!initialized_){
        ROS_ERROR_STREAM("LocalFrameManager is not intialized.");
        return false;
    }

    pos_local_frame.x() = utm.x() - local_frame_utm_x_;
    pos_local_frame.y() = utm.y() - local_frame_utm_y_;
    pos_local_frame.z() = utm.z() - local_frame_msl_;

    return true;
}

bool LocalFrameManager::LocalStaticFrame2UTM(Eigen::Vector3d &pos_local_frame ,Eigen::Vector3d& utm){
    if(!initialized_){
        ROS_ERROR_STREAM("LocalFrameManager is not intialized.");
        return false;
    }

    utm.x() = pos_local_frame.x() + local_frame_utm_x_;
    utm.y() = pos_local_frame.y() + local_frame_utm_y_;
    utm.z() = pos_local_frame.z() + local_frame_msl_;

    return true;
}

bool LocalFrameManager::SetupLocalFrameUsingParams(ros::NodeHandle &n, bool use_dem){

    if (!n.getParam("frame_name", local_static_frame_)){
      ROS_ERROR_STREAM("Could not get the frame name parameter.");
      return false;
    }

    if (!n.getParam("local_frame_lat", local_frame_lat_)){
      ROS_ERROR_STREAM("Could not get the local_frame_lat parameter.");
      return false;
    }


    if (!n.getParam("local_frame_lon", local_frame_lon_)){
      ROS_ERROR_STREAM("Could not get the local_frame_lon parameter.");
      return false;
    }

    local_frame_lat_ = local_frame_lat_*DEG2RAD_C;
    local_frame_lon_ = local_frame_lon_*DEG2RAD_C;

    if (!n.getParam("local_frame_altitude", local_frame_altitude_)){
      ROS_ERROR_STREAM("Could not get the loca_frame_altitude parameter.");
      return false;
    }

    //set utmzone
    if (!n.getParam("UTMZone", utm_zone_)){
      ROS_ERROR_STREAM("Could not get UTMZone parameter.");
      return false;
    }
    // set hemisphere
    std::string hemisphere;
    if (!n.getParam("hemisphere", hemisphere)){
      ROS_ERROR_STREAM("Could not get hemisphere parameter.");
      return false;
    }
    hemisphere_ = hemisphere.at(0);
    coord_conv_.setUTMZone(utm_zone_,hemisphere_);
    coord_conv_.LatLongRad2UTM(local_frame_lat_,local_frame_lon_,&local_frame_utm_x_,&local_frame_utm_y_);


    if(use_dem){
        //set up dem file
        std::string dem_file;
        if (!n.getParam("dem_file", dem_file)){
          ROS_ERROR_STREAM("Could not get dem_file parameter.");
          return false;
        }

        if(!dem_reader_->loadFile(0,dem_file)){
            ROS_ERROR_STREAM("DEM file reading failed.");
            return false;
        }
        bool valid = false;
        local_frame_msl_ = dem_reader_->getHeight(local_frame_lat_*RAD2DEG_C,local_frame_lon_*RAD2DEG_C,&valid);
        if(!valid){
            ROS_ERROR_STREAM("Can't find the dem height");
            return false;
        }
    }

    //set publisher of frame manager params
    frame_manager_params_publisher_ = n.advertise<local_frame_manager::FrameManagerParams>
                                        ("/local_frame_manager/parameters", 1);
    initialized_ = true;
    //PublishFrameManagerParams();
    FramePubTimer = n.createTimer(ros::Duration(0.1),&LocalFrameManager::PublishFrameManagerParamsTimer, this);
    return true;
}

bool LocalFrameManager::TransformLatLon2LocalFrame(double lat, double lon, double altitude,
                                                         Eigen::Vector3d &pos_in_local_frame,
                                                         std::string local_frame){
    Eigen::Vector3d utm;
    if(!LatLon2UTM(lat, lon, altitude ,utm))
        return false;

    if(!UTM2LocalStaticFrame(utm, pos_in_local_frame))
        return false;

    if(local_frame.compare(local_static_frame_)!=0){
        tf::StampedTransform stamped_transform; // local static to local frame
        if(!ca::tf_utils::getCoordinateTransform(tf_listener_,local_static_frame_, local_frame,.2, stamped_transform))
            return false;

        tf::Transform transform = stamped_transform;
        ca::tf_utils::transformPoint(transform, pos_in_local_frame);
    }

    return true;
}

bool LocalFrameManager::TransformLocalFrame2LatLon(Eigen::Vector3d pos_in_local_frame,
                                std::string local_frame,
                                double &lat, double &lon, double &altitude){

    if(local_frame.compare(local_static_frame_)!=0){
        tf::StampedTransform stamped_transform; // local static to local frame
        if(!ca::tf_utils::getCoordinateTransform(tf_listener_, local_frame, local_static_frame_, .2, stamped_transform))
            return false;

        tf::Transform transform = stamped_transform;
        ca::tf_utils::transformPoint(transform, pos_in_local_frame);
    }
    Eigen::Vector3d utm;

    if(!LocalStaticFrame2UTM(pos_in_local_frame, utm))
        return false;
    coord_conv_.UTM2LatLongRad(utm.x(),utm.y(),&lat,&lon);

    altitude = -1.0*(utm.z() - local_frame_msl_) + local_frame_altitude_;
    return true;
}

bool LocalFrameManager::SetUpFrameManagerParams(std::string frame, Eigen::Vector3d& pos, double lat, double lon, double altitude,
                                                int UTMZone, char hemisphere, double time_tolerance){
    message_time_tolerance_ = time_tolerance;
    coord_conv_.setUTMZone(UTMZone,hemisphere);
    local_static_frame_ = frame;
    coord_conv_.LatLongRad2UTM(lat,lon,&local_frame_utm_x_,&local_frame_utm_y_);

    local_frame_utm_x_ = local_frame_utm_x_ - pos.x();
    local_frame_utm_y_ = local_frame_utm_y_ - pos.y();

    local_frame_altitude_ = altitude + pos.z();
    coord_conv_.UTM2LatLongRad(local_frame_utm_x_,local_frame_utm_y_,&local_frame_lat_,&local_frame_lon_);

    if(use_dem_){
        bool valid = false;
        local_frame_msl_ = dem_reader_->getHeight(local_frame_lat_*RAD2DEG_C,local_frame_lon_*RAD2DEG_C,&valid);
        if(!valid){
            ROS_ERROR_STREAM("Can't find the dem height");
            return false;
        }
    }
    return true;
}

bool LocalFrameManager::DoMessagesTimesMatch(){
    double latlon_time = last_received_latlon_.header.stamp.toSec();
    double odo_time = last_received_odo_.header.stamp.toSec();
    if(latlon_time==0.0 || odo_time ==0 || std::abs(latlon_time - odo_time)> message_time_tolerance_)
        return false;

    return true;
}

bool LocalFrameManager::Setup(){
    if(!DoMessagesTimesMatch())
        return false;
    double lat = last_received_latlon_.latitude*DEG2RAD_C;
    double lon = last_received_latlon_.longitude*DEG2RAD_C;
    double altitude = last_received_latlon_.altitude;
    std::string frame = last_received_odo_.header.frame_id;
    Eigen::Vector3d pos; pos.x() = last_received_odo_.pose.pose.position.x;
    pos.y() = last_received_odo_.pose.pose.position.y;
    pos.z() = last_received_odo_.pose.pose.position.z;
    if(!SetUpFrameManagerParams(frame, pos, lat, lon, altitude, utm_zone_, hemisphere_,message_time_tolerance_))
        return false;
    return true;
}

void LocalFrameManager::LatLonCallback(const local_frame_manager::LatLonAlt::ConstPtr& msg){
    last_received_latlon_ = *msg;
    if(!initialized_ && Setup()){
        initialized_ = true;
        delete dem_reader_;
    }
    if(initialized_) PublishFrameManagerParams();
}

void LocalFrameManager::PublishFrameManagerParams(){
    if(!initialized_)
        return;

    local_frame_manager::FrameManagerParams msg;
    msg.local_frame = local_static_frame_;
    msg.local_frame_lat = local_frame_lat_;
    msg.local_frame_lon = local_frame_lon_;
    msg.local_frame_altitude = local_frame_altitude_;

    msg.local_frame_utm_x = local_frame_utm_x_;
    msg.local_frame_utm_y = local_frame_utm_y_;
    msg.local_frame_msl = local_frame_msl_;

    msg.hemisphere = hemisphere_;
    msg.UTMZone = utm_zone_;
    msg.time_allowance = message_time_tolerance_;

    msg.header.stamp = ros::Time::now();
    frame_manager_params_publisher_.publish(msg);
}

void LocalFrameManager::PublishFrameManagerParamsTimer(const ros::TimerEvent& event){
    if(!initialized_)
        return;

    local_frame_manager::FrameManagerParams msg;
    msg.local_frame = local_static_frame_;
    msg.local_frame_lat = local_frame_lat_;
    msg.local_frame_lon = local_frame_lon_;
    msg.local_frame_altitude = local_frame_altitude_;

    msg.local_frame_utm_x = local_frame_utm_x_;
    msg.local_frame_utm_y = local_frame_utm_y_;
    msg.local_frame_msl = local_frame_msl_;

    msg.hemisphere = hemisphere_;
    msg.UTMZone = utm_zone_;
    msg.time_allowance = message_time_tolerance_;

    msg.header.stamp = ros::Time::now();
    frame_manager_params_publisher_.publish(msg);
}

void LocalFrameManager::OdoCallback(const nav_msgs::Odometry::ConstPtr& msg){
    last_received_odo_ = *msg;
    if(!initialized_ && Setup()){
        initialized_ = true;
        delete dem_reader_;
    }
    if(initialized_) PublishFrameManagerParams();
}

bool LocalFrameManager::Initialize(bool server, ros::NodeHandle &n)
{
    if(!server){
        frame_manager_params_subscriber_ = n.subscribe("/local_frame_manager/parameters", 1, & LocalFrameManager::FrameManagerCallback, this);
        initialized_ = false;
    }
    else{

        bool use_param_initialization = false;
        if (!n.getParam("use_param_initialization", use_param_initialization)){
          ROS_ERROR_STREAM("Could not get use_param_initialization parameter.");
          return false;
        }

        bool use_dem = false;
        if (!n.getParam("use_dem", use_dem)){
          ROS_ERROR_STREAM("Could not get use_dem parameter.");
          return false;
        }
        use_dem_ = use_dem;

        if(!use_dem){
            if (!n.getParam("local_frame_msl", local_frame_msl_)){
              ROS_ERROR_STREAM("Could not get local_frame_msl parameter.");
              return false;
            }
        }

        if(use_param_initialization){
            if(!SetupLocalFrameUsingParams(n,use_dem)){
                ROS_ERROR_STREAM("Could not initialize through parameters.");
                return false;
            }

        }
        else{
            initialized_ = false;
            //subscribe to lat lon
            std::string lat_lon_topic;
            if (!n.getParam("lat_lon_topic", lat_lon_topic)){
              ROS_ERROR_STREAM("Could not get lat_lon_topic parameter.");
              return false;
            }
            lat_lon_subscriber_ = n.subscribe(lat_lon_topic,1,& LocalFrameManager::LatLonCallback,this);
            //subscribe to odo
            std::string odo_topic;
            if (!n.getParam("odo_topic", odo_topic)){
              ROS_ERROR_STREAM("Could not get odo_topic parameter.");
              return false;
            }
            odometry_subscriber_ = n.subscribe(odo_topic,1,& LocalFrameManager::OdoCallback, this);
            //set utmzone
            if (!n.getParam("UTMZone", utm_zone_)){
              ROS_ERROR_STREAM("Could not get UTMZone parameter.");
              return false;
            }
            //set hemisphere
            std::string hemisphere;
            if (!n.getParam("hemisphere", hemisphere)){
              ROS_ERROR_STREAM("Could not get hemisphere parameter.");
              return false;
            }
            hemisphere_ = hemisphere.at(0);
            //set time allowance
            if (!n.getParam("message_time_tolerance", message_time_tolerance_)){
              ROS_ERROR_STREAM("Could not get message_time_tolerance parameter.");
              return false;
            }
            //set publisher of frame manager params
            frame_manager_params_publisher_ = n.advertise<local_frame_manager::FrameManagerParams>
                                                ("/local_frame_manager/parameters", 1);
            //set up dem file
            if(!use_dem){
                if (!n.getParam("local_frame_msl", local_frame_msl_)){
                  ROS_ERROR_STREAM("Could not get local_frame_msl parameter.");
                  return false;
                }
            }
            else{
                std::string dem_file;
                if (!n.getParam("dem_file", dem_file)){
                  ROS_ERROR_STREAM("Could not get dem_file parameter.");
                  return false;
                }


                if(!dem_reader_->loadFile(0,dem_file)){
                    ROS_ERROR_STREAM("DEM file reading failed.");
                    return false;
                }
            }
        }
    }
    return true;
}


void LocalFrameManager::FrameManagerCallback(const local_frame_manager::FrameManagerParams::ConstPtr& msg){

    local_frame_lat_ = msg->local_frame_lat;
    local_frame_lon_ = msg->local_frame_lon;
    local_frame_altitude_ = msg->local_frame_altitude;

    local_frame_utm_x_ = msg->local_frame_utm_x;
    local_frame_utm_y_ = msg->local_frame_utm_y;
    local_frame_msl_ = msg->local_frame_msl;

    utm_zone_ = msg->UTMZone;
    local_static_frame_ = msg->local_frame;
    hemisphere_ = msg->hemisphere;
    message_time_tolerance_ = msg->time_allowance;
    coord_conv_.setUTMZone(utm_zone_,hemisphere_);
    initialized_ = true;
}

//Constructor -- Initialize
