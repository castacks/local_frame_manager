#include "local_frame_manager/latlon_frame_publisher.h"

bool LatLonFramePublisher::Initialize(ros::NodeHandle &n){
    bool server = false;
    if(!lfm_client_.Initialize(server,n)){
        ROS_ERROR_STREAM("Could not initialize server.");
        return false;
    }

    int num_frames;
    if (!n.getParam("number_of_frames", num_frames)){
      ROS_ERROR_STREAM("Could not get number_of_frames parameter.");
      return false;
    }
    if(num_frames==0){
        ROS_ERROR_STREAM("Zero Frames, exiting");
        return false;
    }

    std::string lat_string="lat_frame";
    std::string lon_string="lon_frame";
    std::string yaw_string="yaw_frame";
    std::string base_frame_string = "base_frame";
    std::string frame_string = "frame";


    for(size_t i=0; i<num_frames; i++){
        std::string temp_lat_string = lat_string;
        std::string temp_lon_string = lon_string;
        std::string temp_yaw_string = yaw_string;
		std::string temp_base_frame_string = base_frame_string;
		std::string temp_frame_string = frame_string;

        {
            std::stringstream ss; ss << i;
            temp_lat_string.append(ss.str());
            temp_lon_string.append(ss.str());
            temp_base_frame_string.append(ss.str());
            temp_frame_string.append(ss.str());
            temp_yaw_string.append(ss.str());
        }
        geometry_msgs::Point p;
        if (!n.getParam(temp_lat_string, p.x)){
          ROS_ERROR_STREAM("Could not get "<< temp_lat_string<<".");
          return false;
        }
        if (!n.getParam(temp_lon_string, p.y)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_string<<".");
          return false;
        }
        pvector_.push_back(p);

        double yaw;
        if (!n.getParam(temp_yaw_string, yaw)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_string<<".");
          return false;
        }
        yaw_vector_.push_back(yaw);

        std::string b_frame;
        if (!n.getParam(temp_base_frame_string, b_frame)){
          ROS_ERROR_STREAM("Could not get "<< temp_lat_string<<".");
          return false;
        }
        base_frame_vector_.push_back(b_frame);

        std::string frame;
        if (!n.getParam(temp_frame_string, frame)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_string<<".");
          return false;
        }
        frame_vector_.push_back(frame);
    }

    //set up dem file
    std::string dem_file;
    if (!n.getParam("dem_file", dem_file)){
      ROS_ERROR_STREAM("Could not get dem_file parameter.");
      return false;
    }

    if(!dem_reader_.loadFile(0,dem_file)){
        ROS_ERROR_STREAM("DEM file reading failed.");
        return false;
    }

    ros::Rate r(10); // 10 hz
    while(!lfm_client_.IsInitialized() && ros::ok()){
        ros::spinOnce(); r.sleep();
    }

    if(!PutLatLonPointsOnGround())
    {
        ROS_ERROR("Failed to put lat lon on ground!");
    }

    return true;
}

bool LatLonFramePublisher::PutLatLonPointsOnGround(){
    for(size_t i=0; i<pvector_.size(); i++){
        geometry_msgs::Point& p = pvector_[i];
        bool valid = false;
        p.z = dem_reader_.getHeight(p.x,p.y,&valid);
        if(!valid){
            ROS_ERROR_STREAM("Cannot look up height of lat::"<<p.x<<":: lon::"<<p.y);
            return false;
        }

        lfm_client_.MSL2Altitude(p.z,p.z);
        Eigen::Vector3d v(0.0,0.0,0.0);
        ros::Rate loop_rate(10);
        while(!lfm_client_.TransformLatLon2LocalFrame((p.x*DEG2RAD_C),(p.y*DEG2RAD_C),p.z,v,base_frame_vector_[i])){
            ROS_ERROR_STREAM("Couldn't transform lat lon to "<<base_frame_vector_[i]);
            ros::spinOnce();
            loop_rate.sleep();
        }
        p.x = v.x(); p.y = v.y(); p.z = v.z();        
    }

    return true;
}

void LatLonFramePublisher::PublishFrames(){
    for(size_t i=0; i< pvector_.size(); i++){
        tf::Transform transform;
        transform.setOrigin( tf::Vector3(pvector_[i].x, pvector_[i].y, pvector_[i].z) );
        tf::Quaternion q;
        q.setRPY(0, 0, yaw_vector_[i]);
        transform.setRotation(q);
        tf_br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), base_frame_vector_[i], frame_vector_[i]));
    }
}
