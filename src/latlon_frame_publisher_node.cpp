#include "local_frame_manager/latlon_frame_publisher.h"

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "latlon_frame_publisher");
    ros::NodeHandle n("~");
    ros::Rate loop_rate(10);
    LatLonFramePublisher lfp;
    if(!lfp.Initialize(n)){
        ROS_ERROR_STREAM("Failed to Initialize!");
    }

    while(ros::ok()){
        lfp.PublishFrames();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
