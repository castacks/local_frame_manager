#include "ros/ros.h"
#include "local_frame_manager/local_frame_manager.h"


int main(int argc, char **argv)
{

  ros::init(argc, argv, "local_frame_manager_client");
  ros::NodeHandle n("~");
  //std::string image_topic= "/front_cam_left/camera_left/image";
  //ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "/testing_camera_interface/camera_rays", 0 );
  LocalFrameManager fm;
  bool server = false;
  fm.Initialize(server,n);
  ros::Rate r(10); // 10 hz

  while(ros::ok()){
    double x = 1.69;
    double y = -5.484;
    double z = -10.002;

    double lat;
    double lon;
    double altitude;

    if(fm.TransformLocalFrame2LatLon(Eigen::Vector3d(x,y,z),"/world",lat,lon,altitude))
        ROS_INFO_STREAM("Lat::Lon::Alt::"<<lat<<"::"<<lon<<"::"<<altitude);

    Eigen::Vector3d pos;
    if(fm.TransformLatLon2LocalFrame(lat,lon,altitude,pos,"/world"))
        ROS_INFO_STREAM("XYZ::"<<pos.x()<<"::"<<pos.y()<<"::"<<pos.z());

    ros::spinOnce();
    r.sleep();
  }
  return 0;
}

